# Mój Projekt

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam eget neque eu quam convallis consequat. Sed auctor nisi vel mauris malesuada, id congue dolor consectetur.

## O Projekcie

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam eget neque eu quam convallis consequat. Sed auctor nisi vel mauris malesuada, id congue dolor consectetur.

### Cel projektu

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam eget neque eu quam convallis consequat. Sed auctor nisi vel mauris malesuada, id congue dolor consectetur.

### Funkcje

- **Przykładowa Funkcja 1**: *To jest kursywa*
- **Przykładowa Funkcja 2**: **To jest pogrubienie**
- **Przykładowa Funkcja 3**: ~~To jest przekreślenie~~

> "Każdy geniusz jest nieco szalony." - Aristoteles

1. Lista numerowana:
   1. Pierwszy punkt
   2. Drugi punkt
   3. Trzeci punkt

- Lista nienumerowana:
   - Element 1
   - Element 2
   - Element 3

```python
# Przykładowy kod Python
def hello_world():
    for i in range(3):
        print("Hello, World!")

hello_world()
